![PlaceableItems Logo](https://i.imgur.com/IIccIS8.png)

# Placeable Items Mod
This mod allows players to place items as 3D models.

#### Please look at the [CurseForge post](https://minecraft.curseforge.com/projects/placeable-items) for details on the current version

____

#### Random links & info

Looking for a status on the **1.14** development? Head to the [1.14.3 branch](https://github.com/Ferdzz/PlaceableItems/tree/1.14.3)

More screenshots are available on the [CurseForge post](https://www.curseforge.com/minecraft/mc-mods/placeable-items/screenshots)

List of items as of version 3.3 (MC 1.12.2) is available on the [temporary wiki](http://ferdzz.github.io/PlaceableItems/). We definitely need help making a better platform for this wiki, and keeping it up to date. If you feel up to the challenge, feel free to let us know! 

We can get in touch with us through the [Discord](https://discordapp.com/invite/nHv7srK)
____

#### Copyrights

All the models and textures included in the source code or the compiled distribution are the sole property of MasterianoX (Vilim Muršić). You are therefore not allowed to copy, distribute, share, modify or alter for public use the models or the textures in any way shape or form unless prior written consent is provided by MasterianoX (Vilim Muršić).