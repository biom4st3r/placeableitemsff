package me.ferdz.placeableitems.block;

import java.util.ArrayList;
import java.util.List;

import me.ferdz.placeableitems.PlaceableItems;
import me.ferdz.placeableitems.block.component.AbstractBlockComponent;
import me.ferdz.placeableitems.block.component.IBlockComponent;
import me.ferdz.placeableitems.init.PlaceableItemsMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderLayer;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.entity.EntityContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateFactory;
import net.minecraft.state.property.IntProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.BlockMirror;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.loot.context.LootContext;

public class PlaceableItemsBlock extends Block {
    //BrewingStandBlock
    //CauldronBlock
    public static final IntProperty ROTATION = Properties.ROTATION;

    //@Nullable
    private Item item;
    private VoxelShape shape;
    private List<IBlockComponent> components;

    public PlaceableItemsBlock() {
        //super(Block.Properties.create(Material.MISCELLANEOUS));
        super(FabricBlockSettings.of(Material.PART).build());

        this.shape = VoxelShapes.fullCube();
        this.components = new ArrayList<>();
    }

    public PlaceableItemsBlock register(String name) {
        //this.setRegistryName(PlaceableItems.MODID, name);
        Registry.register(Registry.BLOCK, new Identifier(PlaceableItems.MODID, name), this);
        //GameRegistry.findRegistry(Block.class).register(this);
        for (IBlockComponent component : this.components) {
            component.register(this, name);
        }
        return this;
    }

    public PlaceableItemsBlock register(String name, Item item) {
        this.item = item;
        PlaceableItemsMap.instance().put(item, this);
        return this.register(name);
    }

    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    public boolean isSimpleFullBlock(BlockState state, BlockView worldIn, BlockPos pos) {
        // TODO: Decide if we want to disable placement in odd places
        // TODO: check if this is the correct override
        // return worldIn.getBlockState(pos.offset(Direction.DOWN)).isSolid();
        return super.isSimpleFullBlock(state, worldIn, pos);
    }

    // region Rendering

    @Override
    public BlockRenderLayer getRenderLayer() {
        
        return BlockRenderLayer.CUTOUT; // Enables transparency & non full block models
    }

    /// Returning 1 here will disable shadow
    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    @Environment(EnvType.CLIENT)
    //@OnlyIn(Dist.CLIENT)
    public float getHardness(BlockState state, BlockView blockReader, BlockPos pos) {
        return 1.0F;
    }

    // endregion

    // region State handling

    @Override
    public BlockState getPlacementState(ItemPlacementContext context) {
        // Calculates the angle & maps it to the rotation state
        BlockState blockState = this.getDefaultState().with(ROTATION, MathHelper.floor((double) (context.getPlayerYaw() * 16.0F / 360.0F) + 0.5D) & 15);
        for (IBlockComponent component : components) {
            blockState = component.getStateForPlacement(context, blockState);
        }
        return blockState;
    }

    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    public BlockState rotate(BlockState state, BlockRotation rot) {
        return state.with(ROTATION, rot.rotate(state.get(ROTATION), 16));
    }

    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    public BlockState mirror(BlockState state, BlockMirror mirrorIn) {
        return state.with(ROTATION, mirrorIn.mirror(state.get(ROTATION), 16));
    }

    @Override
    protected void appendProperties(StateFactory.Builder<Block, BlockState> builder) {
        builder.add(ROTATION);
    }
    // endregion

    // region Item & drop management

    /// Used for the pick item & binding items in inventory
    @Override
    public Item asItem() {
        for (IBlockComponent component : this.components) {
            Item item = component.asItem();
            if (item != null) {
                return item;
            }
        }
        return this.item;
    }

    /// Used to get the itemstacks dropped when breaking the block
    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    public List<ItemStack> getDroppedStacks(BlockState state, LootContext.Builder builder) {
        ArrayList<ItemStack> itemStacks = new ArrayList<>();
        for (IBlockComponent component : this.components) {
            itemStacks.addAll(component.getDrops(state, builder));
        }
        if (this.asItem() != null) {
            itemStacks.add(new ItemStack(this.asItem()));
        }
        return itemStacks;
    }

    // endregion

    // region Bounding box


    @SuppressWarnings("deprecation") // This is fine to override
    @Override

    public VoxelShape getOutlineShape(BlockState state, BlockView worldIn, BlockPos pos, EntityContext context) {
        for (IBlockComponent component : this.components) {
            VoxelShape shape = component.getShape(this.shape, state, worldIn, pos, context);
            if (shape != null) {
                return shape;
            }
        }
        return this.shape;
    }

    public PlaceableItemsBlock setShape(VoxelShape shape) {
        this.shape = shape;
        return this;
    }

    public VoxelShape getShape() {
        return shape;
    }

    // endregion

    // region Components

    private PlaceableItemsBlock addComponent(IBlockComponent component) {
        this.components.add(component);
        return this;
    }

    PlaceableItemsBlock addComponents(Iterable<IBlockComponent> components) {
        for (IBlockComponent component : components) {
            this.addComponent(component);
        }
        return this;
    }

    @SuppressWarnings("deprecation") // This is fine to override
    @Override
    public boolean activate(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockHitResult hit) {
        boolean result = false;
        boolean hadAnImplementation = false;
        for (IBlockComponent component : this.components) {
            try {
                result |= component.onBlockActivated(state, worldIn, pos, player, handIn, hit);
                hadAnImplementation = true;
            } catch (AbstractBlockComponent.NotImplementedException e) {
                // There was no implementation in this component
            }
        }
        if (!hadAnImplementation) {
            return super.activate(state, worldIn, pos, player, handIn, hit);
        } else {
            return result;
        }
    }

    // endregion
}
